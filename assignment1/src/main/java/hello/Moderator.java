package hello;

/**
 * Created by Mayur on 2/28/15.
 */

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;


public class Moderator
{
	@JsonProperty
	private int id;
	
	@JsonProperty
	@NotEmpty(message = "Name cannot be empty. Please enter a name.")
	private String name;
	
	@JsonProperty
	@NotEmpty(message = "Email cannot be empty.")
	@Email(message = "Email format is invalid. Please check.")
	private String email;
	
	@JsonProperty
	@NotBlank(message = "Password cannot be blank.")
	private String password;
	
	@JsonProperty
	private String created_at;

	public Moderator(Moderator mod)
	{
		this.id = mod.id;
		this.name = mod.name;
		this.email = mod.email;
		this.password = mod.password;
		this.created_at = giveDate();
		System.out.println("Moderator Constructor called with ID: "+id);
	}

	public Moderator(@JsonProperty("name") String name, 
			@JsonProperty("email") String email,
			@JsonProperty("password") String password)
	{
		this.name = name;
		this.email = email;
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public String giveDate()
	{
		TimeZone timeZone = TimeZone.getTimeZone("UTC");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		df.setTimeZone(timeZone);
		return(df.format(new Date()));
	}
}