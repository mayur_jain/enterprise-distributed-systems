package hello;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Generated;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Mayur on 2/28/15.
 */

@Generated("org.jsonschema2pojo")

public class Poll {

	@JsonProperty
	private String id;
	
	@JsonProperty
	@NotEmpty(message = "Question cannot be empty for the Poll.")
	private String question;
	
	@JsonProperty
	@NotEmpty(message = "Start Date cannot be empty for Poll.")
	private String started_at;
	@JsonProperty
	
	@NotEmpty(message = "End Date cannot be empty for Poll.")
	private String expired_at;
	@JsonProperty
	
	@NotEmpty(message = "Choice cannot be empty for Poll.")
	private List<String> choice = new ArrayList<String>();
	
	@JsonProperty
	private ArrayList<Integer> results = new ArrayList<Integer>();


	public Poll(
			@JsonProperty("question") String question,
			@JsonProperty("started_at") String started_at,
			@JsonProperty("expired_at") String expired_at,
			@JsonProperty("choice") List<String> choice
			)
			{
				this.question   = question;
				this.started_at = started_at;
				this.expired_at = expired_at;
				this.choice 	= choice;
				for (int i = 0; i < choice.size(); i++) 
				{	results.add(i, 0); 	}
			
			}

	public void addToResults(int choice_index) {
		int current_value = results.get(choice_index);
		++current_value;
		results.remove(choice_index);
		results.add(choice_index, current_value);
	}
	
	public ArrayList<Integer> getResults() {
		return results;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getStarted_at() {
		return started_at;
	}

	public void setStarted_at(String started_at) {
		this.started_at = started_at;
	}

	public String getExpired_at() {
		return expired_at;
	}

	public void setExpired_at(String expired_at) {
		this.expired_at = expired_at;
	}
	public List<String> getChoice() {
		return choice;
	}

	public void setChoice(List<String> choice) {
		this.choice = choice;
	}
	
	
}