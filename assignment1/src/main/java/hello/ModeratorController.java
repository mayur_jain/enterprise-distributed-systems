package hello;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.xml.bind.DatatypeConverter;

/**
 * Created by Mayur on 2/28/15.
 */

@RestController
@RequestMapping("/api/v1")
public class ModeratorController {

	Map<Integer, Moderator> moderator = new HashMap<Integer, Moderator>(); 
	Map<String, Poll> polls_HashMap_withResults = new HashMap<String, Poll>();
	Map<String, Object> polls_HashMap_withoutResults = new HashMap<String, Object>();
	ArrayList<String> pollIdList = new ArrayList<String>();
	Map< Integer, ArrayList<String> > Moderator_Poll_HashMap = new HashMap< Integer, ArrayList<String> >();


	public static int generateID()
	{
		int aNumber = 0; 
		aNumber = (int)((Math.random() * 900000)+100000); 
		//System.out.print((aNumber));
		return aNumber;
	}

	//***************************MODERATOR REST METHODS*************************************

	//Creating a moderator. POST method
	@RequestMapping(value = "/moderators", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	public Moderator createModerators(@Valid @RequestBody Moderator modJson) throws BadRequestException
	{
		modJson.setId(generateID());
		int mod_id = modJson.getId();
		System.out.println("POST Moderator ID: "+mod_id);

		Moderator modObject = new Moderator(modJson);  //copy values from JSON object to Moderator object
		moderator.put(mod_id, modObject);    //insert object into HashMap
		System.out.println("Moderator Object: "+moderator.get(mod_id) );
		return moderator.get(mod_id);
	}


	//VIEW moderator
	@RequestMapping(value = "/moderators/{moderator_id}", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public Moderator getModerators(@PathVariable(value= "moderator_id") int moderator_id, HttpServletRequest servlet) throws MyInternalServerError
	{
		checkUser(servlet);
		if(moderator.containsKey(moderator_id))
		{
			System.out.println("GET Moderator ID: "+moderator_id);
			System.out.println("Moderator Object: "+moderator.get(moderator_id) );
			return moderator.get(moderator_id);
		}
		else 
			throw new MyInternalServerError("Moderator Object Not Found to GET.");
	}

	//UPDATE moderator
	@RequestMapping(value = "/moderators/{moderator_id}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public Moderator putModerator( @PathVariable(value="moderator_id") int moderator_id, @RequestBody Moderator modJson, HttpServletRequest servlet) throws BadRequestException, MyInternalServerError
	{
		checkUser(servlet);
		if(moderator.containsKey(moderator_id))
		{
			System.out.println("\n PUT Moderator: moderator_id: "+moderator_id);
			Moderator updateObj = moderator.get(moderator_id);
			System.out.println(updateObj);
	
			if( !modJson.getEmail().equals(null) && updateObj.getEmail()!=modJson.getEmail() )
				updateObj.setEmail(modJson.getEmail());
	
			if( !modJson.getPassword().equals(null) && updateObj.getPassword()!=modJson.getPassword() )
				updateObj.setPassword(modJson.getPassword());
	
			return moderator.get(moderator_id);
		}
		else 
			throw new MyInternalServerError("Moderator Object Not Found to Update.");

	}

	//***************************POLL REST METHODS*************************************

	public static String generateBase36ID()
	{
		int id = 0; 
		id = (int)((Math.random() * 900000000)+100000000); 
		//System.out.print((aNumber));
		return Integer.toString(id, 36).toUpperCase();
	}


	//Creating a Poll. POST method
	@RequestMapping(value = "/moderators/{moderator_id}/polls", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	public Map<String, Object> postPolls(@PathVariable(value="moderator_id") int moderator_id, @Valid @RequestBody Poll pollJson, HttpServletRequest servlet) throws BadRequestException, MyInternalServerError
	{
		checkUser(servlet);
		if(moderator.containsKey(moderator_id))
		{
			pollJson.setId(generateBase36ID());
			String poll_id = pollJson.getId();
			System.out.println("POST POLL ID: "+poll_id);
			polls_HashMap_withResults.put(poll_id, pollJson);    		//insert object into HashMap
			//System.out.println("New POLL Object: "+polls_HashMap_withResults.get(poll_id) );
	
			pollIdList.add(poll_id);						//add poll id's to Array list  -- Poll Id List
			Moderator_Poll_HashMap.put(moderator_id, pollIdList);  //add moderator_id and list_of_poll_id's to HashMap
			return getPollsWithoutResults(pollJson);
		}
		else
			throw new MyInternalServerError("Moderator Not Found to Create a Poll.");
	}


	//View a Poll without results
	@RequestMapping(value = "/polls/{poll_id}", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Map<String, Object> viewPollsWithoutResults(@PathVariable(value="poll_id") String poll_id) throws MyInternalServerError
	{
		Poll pollObject = givePollObject(poll_id); 	//search poll object using poll_id
		if(pollObject != null)
			return getPollsWithoutResults(pollObject);	// get poll object from HashMap -- polls_HashMap_withoutResult
		else
			throw new MyInternalServerError("Poll Object Not Found.");
	}


	//View a Poll With results
	@RequestMapping(value = "/moderators/{moderator_id}/polls/{poll_id}", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Poll viewPollsWithResults(@PathVariable(value="moderator_id") int moderator_id, @PathVariable(value="poll_id") String poll_id, HttpServletRequest servlet) throws MyInternalServerError 
	{
		checkUser(servlet);
		if(moderator.containsKey(moderator_id))
		{
			ArrayList<String> pollIdsList = Moderator_Poll_HashMap.get(moderator_id);
			if(pollIdsList.contains(poll_id))
				return polls_HashMap_withResults.get(poll_id);
			else 
				throw new MyInternalServerError("Poll Object Not Found");
		}
		else
			throw new MyInternalServerError("Moderator Object Not Found");

	}


	//List All Polls With results
	@RequestMapping(value = "/moderators/{moderator_id}/polls", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public ArrayList<Poll> viewAllPollsWithResults(@PathVariable(value="moderator_id") int moderator_id, HttpServletRequest servlet) throws MyInternalServerError
	{
		checkUser(servlet);
		if(moderator.containsKey(moderator_id))
		{
			ArrayList<String> pollIdsList = Moderator_Poll_HashMap.get(moderator_id);
			ArrayList<Poll> allPollList = new ArrayList<Poll>(); 

			for (String pid : pollIdsList) {
				allPollList.add(polls_HashMap_withResults.get(pid));
			}
			return allPollList;
		}
		else
			throw new MyInternalServerError("Moderator Object Not Found");
	}

	//Delete a Poll
	@RequestMapping(value = "/moderators/{moderator_id}/polls/{poll_id}", method = RequestMethod.DELETE) 
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ResponseBody
	public void deletePoll(@PathVariable(value="moderator_id") int moderator_id, @PathVariable(value="poll_id") String poll_id, HttpServletRequest servlet) throws MyInternalServerError 
	{
		checkUser(servlet);
		if(moderator.containsKey(moderator_id))
		{
			ArrayList<String> pollIdsList = Moderator_Poll_HashMap.get(moderator_id);
			if(pollIdsList.contains(poll_id))
			{
				pollIdsList.remove(poll_id);
				polls_HashMap_withResults.remove(poll_id);
			}
			else 
				throw new MyInternalServerError("Poll Object Not Found");
		}
		else
			throw new MyInternalServerError("Moderator Object Not Found");
	}	

	//Vote a Poll
	@RequestMapping(value = "/polls/{poll_id}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ResponseBody
	public void voteToPoll(@PathVariable(value="poll_id") String poll_id, @PathVariable @RequestParam(value="choice") int choice) 
	{
		Poll pollObject = polls_HashMap_withResults.get(poll_id);
		pollObject.addToResults(choice);
	}	


	public Map<String, Object> getPollsWithoutResults(Poll pollObject)
	{
		polls_HashMap_withoutResults.put("id", pollObject.getId());
		polls_HashMap_withoutResults.put("question", pollObject.getQuestion());
		polls_HashMap_withoutResults.put("started_at", pollObject.getStarted_at());
		polls_HashMap_withoutResults.put("expired_at", pollObject.getExpired_at());
		polls_HashMap_withoutResults.put("choice", pollObject.getChoice());

		return polls_HashMap_withoutResults;
	}

	public Poll givePollObject(String pid){
		return polls_HashMap_withResults.get(pid);
	}


	
	public void checkUser(HttpServletRequest req)
	{
	    String auth = req.getHeader("Authorization");

	    if(auth == null)
	        throw new AuthException("Please provide username and password to access system");

	    String credentials = auth.substring("Basic".length()).trim();
	    byte[] decoded = DatatypeConverter.parseBase64Binary(credentials);
	    String decodedString = new String(decoded);
	    String[] originalCredentials = decodedString.split(":");

	    if(originalCredentials.length < 2)
	        throw new AuthException("Username Or Password is Empty");

	    String ID = originalCredentials[0];
	    String Password = originalCredentials[1];

	    if(!(ID.equals("foo") && Password.equals("bar"))){
	        throw new AuthException("Authentication Error!");
	    }

	}

}


