package hello;

/**
 * Created by Mayur on 2/30/15.
 */

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
class MyInternalServerError extends Exception {
 
    private String message = null;
 
    public MyInternalServerError() {
        super();
    }
 
    public MyInternalServerError(String message) {
        super(message);
        this.message = message;
    }
 
    public MyInternalServerError(Throwable cause) {
        super(cause);
    }
 
    @Override
    public String toString() {
        return message;
    }
 
    @Override
    public String getMessage() {
        return message;
    }  

}