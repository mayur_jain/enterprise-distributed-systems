package WebVotingApp.Controller_Services;

import WebVotingApp.Model.Moderator;
import WebVotingApp.Model.Poll;
import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

@Component
public class Kafka_Producer
{
    @Autowired
    private MongoTemplate mongoTemplate;

    public static final String COLLECTION_POLL = "poll";
    public static final String COLLECTION_MODERATOR = "moderator";

    private static Producer<Integer, String> producer;
    private final Properties properties = new Properties();

    String topic = "cmpe273-new-topic";

    public Kafka_Producer(){
        properties.put("metadata.broker.list", "54.68.83.161:9092");
        properties.put("serializer.class", "kafka.serializer.StringEncoder");
        properties.put("request.required.acks", "1");
        producer = new Producer<>(new ProducerConfig(properties));
    }

    @Scheduled(fixedDelay =30000,initialDelay=1000)
    public void sendMessage() throws Exception
    {
        new Kafka_Producer();

        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Date currentDate = new Date();

        BasicQuery find_all_expired_polls = new BasicQuery("{expiryNotified:false},{id:1,expiredAt:1,results:1}");
        ArrayList<Poll> expired_polls = (ArrayList<Poll>) mongoTemplate.find(find_all_expired_polls, Poll.class,COLLECTION_POLL);

        for(Poll poll: expired_polls)
        {
            Date expiry_date = formatter.parse(poll.getExpiredAt());
            if(expiry_date.before(currentDate))
            {
                BasicQuery find_oderator_with_pollid = new BasicQuery("{polls:\""+poll.getId()+"\"},{email:1}");
                String moderator_email = mongoTemplate.findOne(find_oderator_with_pollid, Moderator.class, COLLECTION_MODERATOR).getEmail();

                StringBuilder msg_content = new StringBuilder();
                msg_content.append(moderator_email);
                msg_content.append(":009991059:");
                msg_content.append("Poll Result [" + poll.getChoice_Result() + "]");

                KeyedMessage<Integer, String> data = new KeyedMessage<>(topic,msg_content.toString());
                producer.send(data);        //send  message

                poll.setExpiryNotified(true);      //set notified flag to true
                mongoTemplate.save(poll, COLLECTION_POLL);     //update poll object
            }
        }
        
        producer.close();
    }
}
