package WebVotingApp.Controller_Services;

import WebVotingApp.AppController.BadRequestException;
import WebVotingApp.Model.Moderator;
import WebVotingApp.Model.Poll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Repository
public class ServiceMethods {

    public static final String COLLECTION_Moderator = "moderator";
    public static final String COLLECTION_Poll = "poll";

    @Autowired
    private MongoTemplate mongoTemplate;

    //genetrate alphanumericID
    public static String generateBase36ID()
    {
        int id = 0;
        id = (int)((Math.random() * 900000000)+100000000);
        //System.out.print((aNumber));
        return Integer.toString(id, 36).toUpperCase();
    }


    public static long generateID()
    {
        long range = 1234567L;
        Random r = new Random();
        long number = (long) (r.nextDouble() * range);

        return number;
    }


    public Moderator addModerator(Moderator moderator)
    {
        Moderator m = new Moderator(generateID(), moderator.getName(), moderator.getEmail(), moderator.getPassword(), getCurrentDate());

        if (!mongoTemplate.collectionExists(Moderator.class))
            mongoTemplate.createCollection(Moderator.class);

        mongoTemplate.insert(m, COLLECTION_Moderator);
        
        return m;
    }


    public Moderator findModerator(long i)
    {
        Moderator m;
        if (mongoTemplate.collectionExists(Moderator.class)) {
            m = mongoTemplate.findById(i, Moderator.class, COLLECTION_Moderator);
            if (m == null)
                throw new BadRequestException("Moderator Object Not Found.");
        }
        else
            throw new BadRequestException("Moderator Collection not found!");
        return m;
    }


    public Moderator updateModeratorAt(long id, Moderator mod)
    {
        Moderator m = findModerator(id);

        m.setEmail(mod.getEmail());
        m.setPassword(mod.getPassword());

        mongoTemplate.save(m, COLLECTION_Moderator);

        return mongoTemplate.findById(id, Moderator.class, COLLECTION_Moderator);
    }

    public String getCurrentDate() {
        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        df.setTimeZone(timeZone);
        return (df.format(new Date()));
    }


    public Map<String, Object> createPoll(Poll poll, long moderatorId)
    {
        Poll p = new Poll(generateBase36ID(), poll.getQuestion(), poll.getStartedAt(), poll.getExpiredAt(), poll.getChoice());

        Moderator m;
        if (mongoTemplate.collectionExists(Moderator.class))
        {
            m = mongoTemplate.findById(moderatorId, Moderator.class, "moderator");
            if (m == null)
                throw new BadRequestException("Moderator Object Not Found.");
        }
        else
            throw new BadRequestException("Moderator Collection not found!");

        mongoTemplate.insert(p, COLLECTION_Poll);
        m.addPoll(p.getId());
        mongoTemplate.save(m, "moderator");

        return mongoTemplate.findById(p.getId(), Poll.class, COLLECTION_Poll).withoutResult();

    }


    public Map<String, Object> getPoll(String pollId) {

        Poll p = mongoTemplate.findById(pollId, Poll.class, COLLECTION_Poll);

        if (p == null)
            throw new BadRequestException("Poll NOT found with this id.");

        return p.withoutResult();
    }



    public Map<String, Object> getPollWithModerator(long moderatorId, String pollId) {

        if (mongoTemplate.collectionExists(Moderator.class)) {
            if (mongoTemplate.findById(moderatorId, Moderator.class, "moderator") == null)
                throw new BadRequestException("Moderator Object Not Found.");
        }
        else
            throw new BadRequestException("Moderator Collection Not found!");

        Poll poll = mongoTemplate.findById(pollId, Poll.class, COLLECTION_Poll);

        if (poll == null)
            throw new BadRequestException("Poll NOT found with this id.");

        return poll.withResult();
    }



    public ArrayList<Map<String, Object>> getPollsWithModerator(long moderatorId) {

        Moderator m;
        if (mongoTemplate.collectionExists(Moderator.class))
        {
            m = mongoTemplate.findById(moderatorId, Moderator.class, "moderator");
            if (m == null)
                throw new BadRequestException("Moderator Object Not Found.");
        }
        else
            throw new BadRequestException("Moderator Collection Not found!");

        ArrayList<String> pollIds = m.getPolls();
        if (pollIds.size() == 0) {
            throw new BadRequestException("Moderator does NOT have any Poll. Please check.");
        }

        ArrayList<Map<String, Object>> map = new ArrayList<>();
        for (String pollId : pollIds)
        {
            map.add(getPollWithModerator(moderatorId, pollId));
        }
        return map;

    }


    public void deletePoll(long moderatorId, String pollId)
    {
        Moderator m;

        if (mongoTemplate.collectionExists(Moderator.class))
        {
            m = mongoTemplate.findById(moderatorId, Moderator.class, "moderator");
            if (m == null)
                throw new BadRequestException("Moderator Object Not Found.");
        }
        else
            throw new BadRequestException("Moderator Collection not found!");

        Poll poll = mongoTemplate.findById(pollId, Poll.class, COLLECTION_Poll);

        if (poll == null)
            throw new BadRequestException("Moderator does NOT have this Poll. Please check.");

        m.removePoll(pollId);
        mongoTemplate.save(m, "moderator");     //update the moderator object
        mongoTemplate.remove(poll, COLLECTION_Poll);

    }


    public void votePoll(String pollId, int choiceno)
    {

        Poll poll = mongoTemplate.findById(pollId, Poll.class, COLLECTION_Poll);

        if (poll == null)
            throw new BadRequestException("Poll NOT found with this id");

        if (poll.getChoice().size() <= choiceno)
            throw new BadRequestException("Invalid choice");

        poll.voteChoice(choiceno);
        mongoTemplate.save(poll, COLLECTION_Poll);
    }


}
