package WebVotingApp.AppController;


import WebVotingApp.Model.Moderator;
import WebVotingApp.Model.Poll;
import WebVotingApp.Controller_Services.ServiceMethods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.xml.bind.DatatypeConverter;
import java.util.ArrayList;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class AppController
{

    @Autowired
    ServiceMethods service = new ServiceMethods();

    //***************************MODERATOR REST APIs*************************************

    //Creating a moderator. POST method
    @RequestMapping(value = "/moderators", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String,Object> postModerator(@Valid @RequestBody Moderator moderator,BindingResult result,HttpServletRequest servlet)
    {
        if(moderator.getName()==null || moderator.getName().trim().equals(""))
            throw new BadRequestException("Bad Request Exception");
        if(result.hasErrors())
            throw new BadRequestException("Bad Request Exception");

        return service.addModerator(moderator).toMapWithOutResult();
    }

    //VIEW moderator
    @RequestMapping(value={"/moderators/{id}"},method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> getModerators(@PathVariable(value="id") long id,HttpServletRequest servlet)
    {
        checkUser(servlet);
        return service.findModerator(id).toMapWithOutResult();
    }


    //UPDATE moderator
    @RequestMapping(value={"/moderators/{id}"},method=RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> putModerator(@PathVariable(value="id") long id,@Valid @RequestBody Moderator moderator, BindingResult result,HttpServletRequest servlet)
    {
        checkUser(servlet);
        if(result.hasErrors())
            throw new BadRequestException("Bad Request");

        return service.updateModeratorAt(id,moderator).toMapWithOutResult();
    }

    //***************************POLL REST APIs*************************************

    //Creating a Poll. POST method
    @RequestMapping(value={"/moderators/{id}/polls"},method=RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String, Object> postPolls(@PathVariable(value="id") long moderatorId,@Valid @RequestBody Poll poll,BindingResult result,HttpServletRequest servlet)
    {
        checkUser(servlet);
        if(result.hasErrors())
        {
            throw new BadRequestException("Bad Request");
        }
        return service.createPoll(poll,moderatorId);
    }


    //View a Poll without results
    @RequestMapping(value={"/polls/{poll_id}"},method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Map<String, Object> viewPollWithoutResults(@PathVariable(value = "poll_id") String pollId)
    {
        return service.getPoll(pollId);
    }


    //View a Poll With results
    @RequestMapping(value={"/moderators/{moderator_id}/polls/{poll_id}"},method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Map<String, Object> viewPollWithResults(@PathVariable(value="moderator_id") long moderatorId,@PathVariable(value="poll_id") String pollId,HttpServletRequest servlet)
    {
        checkUser(servlet);
        return service.getPollWithModerator(moderatorId,pollId);
    }


    //List All Polls With results
    @RequestMapping(value={"/moderators/{moderator_id}/polls"},method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public ArrayList<Map<String, Object>> viewAllPollsWithResults(@PathVariable(value = "moderator_id") long moderatorId, HttpServletRequest servlet) {
        checkUser(servlet);
        return  service.getPollsWithModerator(moderatorId);
    }


    //Delete a Poll
    @RequestMapping(value={"/moderators/{moderator_id}/polls/{poll_id}"},method=RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletePoll(@PathVariable(value="moderator_id") long moderatorId,@PathVariable(value="poll_id") String pollId,HttpServletRequest servlet) {
        checkUser(servlet);
        service.deletePoll(moderatorId, pollId);
    }


    //Vote a Poll
    @RequestMapping(value={"/polls/{poll_id}"},method=RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void voteAPoll(@PathVariable(value = "poll_id") String pollId,@RequestParam("choice")int choiceno) {
        service.votePoll(pollId,choiceno);
    }


    //basic Auth validation check
    public static void checkUser(HttpServletRequest req) {
        String authorization = req.getHeader("Authorization");

        if (authorization == null)
            throw new AuthException("Please provide username and password to access system");

        String credentials = authorization.substring("Basic".length()).trim();
        byte[] decoded = DatatypeConverter.parseBase64Binary(credentials);
        String decodedString = new String(decoded);
        String[] actualCredentials = decodedString.split(":");

        if (actualCredentials.length < 2)
            throw new AuthException("Username Or Password is Empty");

        String ID = actualCredentials[0];
        String Password = actualCredentials[1];

        if (!(ID.equals("foo") && Password.equals("bar"))) {
            throw new AuthException("Authentication Error!");
        }

    }


}